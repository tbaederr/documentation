---
title: LTP update procedure
---

Instead of using [upstream's LTP] directly, we have our own copy of
LTP for a commit we trust. This is to avoid that breakage in LTP's
`master` branch can make our test suites less stable. But this also
means that we need to periodically update this commit so we can pull
in recent fixes and updated tests from LTP repository.

To update our copy of LTP, we internally follow these steps:

* Create a branch on your fork of [kernel-tests] (_create a fork if you
  don't have one already_).
* Look for a recent commit in the [upstream LTP repo] **that passes
  all Travis checks** and update the commit hash in the `git checkout`
  command in [runtest.sh] (global variable `LTP_REPO_COMMIT_ID`) to use that
  commit.
* If there are any known issues that are reportedly now fixed
  (e.g. tests that now can run because they have been improved, or
  because kernel bugs have been fixed), drop them from
  [knownissue.sh].
* Push the branch with your changes with comment
  `Update LTP to a recent commit`.
* File an MR to repo [kernel-tests].
* Test your branch via `cki-ci-bot` in your MR, e.g.

  ```plain
  @cki-ci-bot, please test [skip_beaker=false][baseline/upstream-stable][tests_regex=.*LTP.*]
  ```

[upstream's LTP]: https://github.com/linux-test-project/ltp
[kernel-tests]: https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests
[upstream LTP repo]: https://github.com/linux-test-project/ltp/commits/master
[runtest.sh]: https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/blob/main/distribution/ltp-upstream/lite/runtest.sh
[knownissue.sh]: https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/blob/main/distribution/ltp-upstream/include/knownissue.sh
